<?php
/**
 * An object for setting-up hook expectations on stub modules in a fluent
 * manner.
 */
class DrupalMockeryStubModuleExpectationBuilder {
  /**
   * @var \DrupalMockery
   */
  protected $mockery;

  /**
   * @var string
   */
  protected $moduleName;

  /**
   * Constructor for DrupalMockeryStubModuleExpectationBuilder.
   *
   * Initializes a new fluent expectation builder for the specified stub module.
   * The stub module must already exist and implement the appropriate hooks as
   * stubs in order for the expectations this builder sets up to be effective.
   *
   * @param \DrupalMockery $mockery
   *   The Drupal-specific mockery that is managing the test setup.
   * @param string $moduleName
   *   The machine name of the stub module.
   */
  public function __construct(\DrupalMockery $mockery, $moduleName) {
    $this->mockery = $mockery;
    $this->moduleName = $moduleName;
  }

  /**
   * Sets an expectation that the module should receive the hook with the
   * specified name.
   *
   * Since this method returns a Mockery expectation, you will need to invoke
   * this method separately for each hook that the stub module is expected to
   * receive during the test. In other words, you cannot chain multiple hook
   * expectations in the same call flow.
   *
   * @param string $hookName
   *   The name of the hook for which an expectation is being set (e.g. for
   *   <code>hook_menu</code>, the value passed for this parameter would be
   *   "menu").
   *
   * @return \Mockery\Expectation
   *   An expectation object from Mockery, for chaining calls to setup
   *   additional expectations on the hook invocation, including arguments,
   *   return value, etc.
   */
  public function shouldReceiveHook($hookName) {
    $globalMock = $this->mockery->getGlobalFunctionMock();
    $hookFunctionName = $this->moduleName . '_' . $hookName;

    return $globalMock->shouldReceive($hookFunctionName);
  }
}
