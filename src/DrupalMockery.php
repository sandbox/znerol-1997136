<?php
use \Mockery\MockInterface;

/**
 * A factory for Drupal-specific Mockery mocks.
 *
 * You will typically only need one instance of this class, which you would
 * construct in the <code>setUp()</code> method of your
 * <code>DrupalWebTestCase</code>, as follows:
 * @code
 * class MockeryTestCase extends DrupalWebTestCase {
 *   protected $mockery;
 *   ...
 *   public function setUp() {
 *     ...
 *     $this->mockery = DrupalMockery::mockery($this->databasePrefix);
 *     ...
 *   }
 *   ...
 * }
 * @endcode
 *
 * You will also want to ensure that all mock objects verify their expectations
 * in the tear-down, as follows:
 * @code
 * class MockeryTestCase extends DrupalWebTestCase {
 *   ...
 *   public function tearDown() {
 *     ...
 *     $this->mockery->verifyAndTearDown($this);
 *     parent::tearDown();
 *     ...
 *   }
 *   ...
 * }
 * @endcode
 *
 * @see DrupalMockery::mockery()
 */
class DrupalMockery {
  /**
   * The name of the mock "object" that simulates PHP's global namespace by
   * receiving hook calls and verifying them against expectations.
   */
  const GLOBAL_MOCK_NAME = '_global';

  protected static $instances = array();
  protected $data;
  protected $name;
  protected $needsRestore;

  //============================================================================
  // Static Factory Methods
  //============================================================================

  /**
   * Creates or returns an existing factory for Drupal-specific Mockery mocks.
   *
   * If a mock factory with the specified name does not exist, one will be
   * created before being returned.
   *
   * @param string $name
   *   The unique identifier for the mock factory to return. In most cases,
   *   this should be the database prefix SimpleTest is using for the current
   *   test case.
   *
   * @return \DrupalMockery
   *   A Drupal-aware Mockery factory.
   */
  public static function mockery($name) {
    if (!isset(static::$instances[$name])) {
      static::$instances[$name] = new DrupalMockery($name);
    }

    return static::$instances[$name];
  }

  /**
   * Default implementation of a hook stubbed for test purposes.
   *
   * Use this in stub modules to implement stubbed hooks, as follows:
   * @code
   * function MY_MODULE_some_hook() {
   *   return DrupalMockery::hookStub(__FUNC__, func_get_args());
   * }
   * @endcode
   *
   * @param string $func
   *   The name of the hook function.
   *
   * @return mixed
   *   Whatever the mock for the hook returns.
   */
  public static function hookStub($func, $args) {
    $mockery = DrupalMockery::mockery(drupal_valid_test_ua());

    $mock = $mockery->getGlobalFunctionMock();
    $mockHook = array($mock, $func);

    return call_user_func_array($mockHook, $args);
  }

  /**
   * Constructor for the DrupalMockery mock factory.
   *
   * This is protected so that it is not called directly by tests. Instead,
   * tests are encouraged to re-use existing instances via the static factory
   * method this class provides.
   *
   * @see DrupalMockery::mockery()
   *
   * @param string $name
   *   The unique identifier for the mock factory in the test. In most cases,
   *   this should be the database prefix SimpleTest is using for the current
   *   test case.
   */
  protected function __construct($name) {
    $this->name = $name;
    $this->data = array();
    $this->needsRestore = FALSE;

    $this->setupGlobalFunctionMock();
  }

  /**
   * Creates a new mock object that is identified by the given label.
   *
   * This method accepts a variable number of arguments. All arguments past the
   * first argument are passed through to <code>\Mockery::mock()</code>. If
   * only <code>$label</code> is provided, the default is to re-use the label as
   * the only argument to <code>\Mockery::mock()</code>.
   *
   * This means that you can pass the name of a class or interface as the label
   * to mock the corresponding class or interface, respectively, per
   * @link http://docs.mockery.io/en/stable/reference/startup_methods.html the Mockery docs @endlink.
   *
   * For example:
   * @code
   * $drupalMockery->mock('\\EntityInterface');
   * @endcode
   *
   * Would mock the EntityInterface and is equivalent to the following native
   * Mockery code:
   * @code
   * \Mockery::mock('\\EntityInterface');
   * @endcode
   *
   * Now, consider the following code:
   * @code
   * $drupalMockery->mock('myMock', array('foo' => 1,'bar' => 2));
   * @endcode
   *
   * This mocks an anonymous object that returns <code>1</code> when
   * <code>foo()</code> is called, and <code>2</code> when <code>bar</code> is
   * called. It is equivalent to the following native Mockery code:
   * @code
   * \Mockery::mock(array('foo' => 1,'bar' => 2));
   * @endcode
   *
   * The mock object is wrapped in a proxy object that transparently handles
   * loading of mock classes before method calls are forwarded to them.
   *
   * @see \DrupalMockeryMockProxy
   *
   * @param string $label
   *   A unique label for the mock object.
   * @param mixed... $mockArgs
   *   Optional arguments to pass to Mockery when constructing the mock. If not
   *   provided, defaults to passing <code>$label</code> as the argument to
   *   <code>\Mockery::mock()</code>.
   *
   * @return \DrupalMockeryMockProxy
   *   A proxy object that can be used the same was as the underlying mock.
   */
  public function mock($label) {
    $funcArgs = func_get_args();

    if (count($funcArgs) > 1) {
      $mockArgs = array_slice($funcArgs, 1);
    }
    else {
      // Default to passing the label as the only arg
      $mockArgs = array($label);
    }

    $mockFactoryMethod = array('\\Mockery', 'mock');
    $mock = call_user_func_array($mockFactoryMethod, $mockArgs);
    $this->setMock($label, $mock);

    $proxy = new DrupalMockeryMockProxy($this->name, $label);
    $this->data['proxies'][$label] = $proxy;

    return $proxy;
  }

  /**
   * Gets the mocks that have been generated or registered with this factory.
   *
   * Be aware that mocks may not be fully loaded and ready for use. Always
   * run a mock through fullyLoadMock() before using it directly.
   *
   * @return \Mockery\Mock[]
   *   The mocks that this factory is aware of.
   */
  public function getMocks() {
    return $this->data['mocks'];
  }

  /**
   * Gets the labels of the mocks generated or registered with this factory.
   *
   * @return string[]
   *   The labels of all mocks this factory is aware of.
   */
  public function getMockLabels() {
    return array_keys($this->getMocks());
  }

  /**
   * Determine if a mock with the given label is registered with this factory.
   *
   * @param string $label
   *   The label of the desired mock.
   *
   * @return bool
   *   TRUE if the mock is registered; FALSE if it is not.
   */
  public function hasMock($label) {
    $mocks = $this->getMocks();

    return isset($mocks[$label]);
  }

  /**
   * Gets the existing mock object with the given label.
   *
   * Before the mock is returned, the necessary base and mock classes are
   * automatically defined and loaded if they are not yet defined in the current
   * request.
   *
   * @param string $label
   *   The label of the desired mock.
   *
   * @return MockInterface
   *   The Mockery mock object.
   *
   * @throws \InvalidArgumentException
   *   If no mock object with the specified label was previously created with
   *   this factory.
   */
  public function getMock($label) {
    if (!$this->hasMock($label)) {
      throw new InvalidArgumentException(
        'No mock object with the specific label has been created with this ' .
        'mock factory: ' . $label);
    }

    $mocks = $this->getMocks();
    $mock  = $mocks[$label];

    $mockLoader = new DrupalMockeryIncompleteClassReifier();
    $loadedMock = $mockLoader->fullyLoadMock($mock);

    if ($mock !== $loadedMock) {
      $mock = $loadedMock;

      $this->setMock($label, $mock);
    }

    return $mock;
  }

  /**
   * Stores a mock object under the provided label in this mock factory.
   *
   * @param string $label
   *   The label for the mock to save.
   * @param MockInterface $mock
   *   The mock object.
   */
  protected function setMock($label, $mock) {
    $this->data['mocks'][$label] = $mock;
  }

  /**
   * Gets the mock object used for functions that live outside of classes.
   *
   * @return MockInterface
   */
  public function getGlobalFunctionMock() {
    return $this->getMock(\DrupalMockery::GLOBAL_MOCK_NAME);
  }

  /**
   * Fluent builder method for setting up hook expectations for a module.
   *
   * The provided callback is invoked with a builder that can be used to setup
   * the hook expectations, as follows:
   * @code
   * $this->mockery->setupModule('my_stub_module', function($module, $mockery) {
   *   $module
   *     ->shouldReceiveHook('my_hook')->once()
   *     ->andReturn(array(
   *       t('Easily test drupal hooks'),
   *       t('Smart integration into simpletest')
   *     ));
   * });
   * @endcode
   *
   * In the example above, the module named <code>my_stub_module</code> must
   * exist and implement a stub for <code>hook_my_hook</code> (i.e. the function
   * must be defined <code>my_stub_module_my_hook</code> and it must invoke
   * <code>DrupalMockery::hookStub()</code>.
   *
   * For reference, <code>$module</code> is of type
   * <code>\DrupalMockeryStubModuleExpectationBuilder</code>, and
   * <code>$mockery</code> is of type <code>\DrupalMockery</code>.
   *
   * @see \DrupalMockery::hookStub()
   *
   * @param string $moduleName
   *   The name of the stub module for which expectations are being setup.
   *
   * @param callable $callback
   *   The function callback or anonymous function that
   *
   * @return \DrupalMockery
   *   This object, for chaining calls.
   */
  public function setupModule($moduleName, $callback) {
    $expectationBuilder =
      new \DrupalMockeryStubModuleExpectationBuilder($this, $moduleName);

    $this->restore();
    call_user_func($callback, $expectationBuilder, $this);
    $this->save();

    return $this;
  }

  /**
   * Restores the state of this mock factory from the Drupal bootstrap cache.
   *
   * The state is only restored if necessary. If this method is called with no
   * arguments, and the state of this method has not previously been saved, then
   * calling this method has no effect.
   *
   * @param boolean $force_restore
   *   An optional parameter that controls whether to restore state from the
   *   cache, regardless of whether or not the factory requires its state to be
   *   restored.
   */
  public function restore($force_restore = FALSE) {
    if ($this->needsRestore || $force_restore) {
      $cacheKey = $this->getCacheKey();
      $cache = cache_get($cacheKey, 'cache_bootstrap');

      if (!empty($cache)) {
        $newData = $cache->data;

        $mocks = $newData['mocks'];
        $loadedMocks = $this->loadMocksAndClosures($mocks);
        $newData['mocks'] = $loadedMocks;

        $this->data = $newData;
      }

      $this->needsRestore = FALSE;
    }
  }

  /**
   * Saves the state of this mock factory into the Drupal bootstrap cache.
   */
  public function save() {
    $tlds_available = function_exists('tlds_log');

    $cacheKey = $this->getCacheKey();
    $mocks = $this->data['mocks'];

    // If the appropriate support is available, prepare closures for
    // serialization to avoid PHP errors.
    //
    // This should only affect uses of the with(on()) and andReturnUsing()
    // expectations.
    if (\DrupalMockeryClosureSerializer::isSupported()) {
      // Serialization happens "in-place" within each mock.
      // This replaces each closure with NULL so the mocks can serialize.
      $serializer = \DrupalMockeryClosureSerializer::getInstance();
      $serializer->serializeClosuresInMocks($mocks);
    }
    elseif ($tlds_available) {
      tlds_log(
        'Closure serialization is not available. If closures are used in mock '.
        'expectations, mock serialization will fail.');
    }

    try {
      // If this fails, something in a mock is still not serializable
      // (like a closure or SimpleXML element).
      cache_set($cacheKey, $this->data, 'cache_bootstrap');
    }
    catch (\Exception $ex) {
      $error   = $ex->getMessage();
      $trace   = $ex->getTraceAsString();

      $message = "Unable to serialize and save mock state: {$error}\n{$trace}";

      if ($tlds_available) {
        tlds_log($message);
        tlds_log($this->data, 'DrupalMockery state');
      }

      // Ensure this always appears in the test results.
      debug($message);
    }

    // FIXME: This is inefficient; we need a better way to omit closures from
    // the mocks before being serialized.
    if (isset($serializer)) {
      // Put the closures back where they were in each mock.
      $serializer->deserializeClosuresInMocks($mocks);
    }

    // Ensures that we restore before trying to work with any mocks, since once
    // our data has been written out, it can start getting manipulated by other
    // requests.
    $this->needsRestore = TRUE;
  }

  /**
   * Verifies expectations on all mock objects and then clears its mock state.
   *
   * @param \DrupalTestCase $testCase
   *   The test case that is currently being run. This test case is notified
   *   of failure if any mock expectation is not satisfied.
   */
  public function verifyAndTearDown(\DrupalTestCase $testCase) {
    try {
      $this->verify($testCase);
    }
    finally {
      $this->tearDown();
    }
  }

  /**
   * Verifies expectations on all mock objects created by this factory.
   *
   * @param \DrupalTestCase $testCase
   *   The test case that is currently being run. This test case is notified
   *   of failure if any mock expectation is not satisfied.
   */
  public function verify(\DrupalTestCase $testCase) {
    // Prevent serialization/deserialization from killing the test framework
    try {
      $this->restore();
    }
    catch (\Exception $ex) {
      $error = $ex->getMessage();
      $trace = $ex->getTraceAsString();

      $this->failTestCase(
        $testCase,
        "Unable to restore mock state: {$error}\n{$trace}");

      return;
    }

    foreach ($this->getMockLabels() as $label) {
      try {
        $mock = $this->getMock($label);
        $mock->mockery_verify();
      }
      // NOTE: This does not extend from \Mockery\Exception in 0.9.9
      catch (\Mockery\CountValidator\Exception $ex) {
        $this->failTestCase($testCase, $ex->getMessage());
      }
      catch (\Mockery\Exception $ex) {
        $this->failTestCase($testCase, $ex->getMessage());
      }
    }

    $this->save();
  }

  /**
   * Purges all mocks in the container for this mockery.
   */
  public function tearDown() {
    $globalMock = $this->getGlobalFunctionMock();
    $container  = $globalMock->mockery_getContainer();

    $container->mockery_close();
    \Mockery::resetContainer();

    $this->data = array();
    $this->needsRestore = FALSE;
  }

  /**
   * Ensures that the classes for all mocks are defined, and closures are ready.
   *
   * This converts mocks that may have loaded as `__PHP_Incomplete_Class` into
   * the appropriate type, and then leverages Super Closure (if available) to
   * deserialize closures in-place with the mocks.
   *
   * @param array $mocks
   *   The mocks to load.
   *
   * @return array
   *   The loaded mocks.
   */
  protected function loadMocksAndClosures(array $mocks) {
    $loadedMocks = array();

    $mockLoader = new DrupalMockeryIncompleteClassReifier();

    // Fully load all mocks before we touch closures. We can't deserialize
    // mocks inside partial objects.
    foreach ($mocks as $label => $mock) {
      $loadedMocks[$label] = $mockLoader->fullyLoadMock($mock);
    }

    // If the appropriate support is available, unpack serialized closures.
    if (\DrupalMockeryClosureSerializer::isSupported()) {
      // De-serialization happens "in-place" within each mock
      $serializer = \DrupalMockeryClosureSerializer::getInstance();
      $serializer->deserializeClosuresInMocks($loadedMocks);
    }

    return $loadedMocks;
  }

  /**
   * Invokes <code>fail()</code> on a test case, with passed args.
   *
   * All arguments beyond the first argument are passed-through to the
   * <code>fail()</code> method, unaltered.
   *
   * This is a workaround for the fact that <code>\DrupalTestCase::fail()</code>
   * is protected. Alternatives to this method were:
   *   1) Require the test case to declare the fail method as public.
   *   2) Require the test case to pass in an anonymous "failure" function that
   *      can be invoked to call fail().
   *   3) Patch Drupal core to make fail() public.
   *
   * The current approach minimizes the amount of code that the test case has
   * to implement in order to use Mockery, while also avoiding a mandatory
   * patch to Drupal core.
   *
   * @see DrupalTestCase::fail()
   *
   * @param \DrupalTestCase $testCase
   *   The test case being executed.
   * @param mixed... $args
   *   The arguments to pass to fail().
   */
  protected function failTestCase(\DrupalTestCase $testCase) {
    $failArgs = array_slice(func_get_args(), 1);

    $testCaseReflector = new \ReflectionClass($testCase);

    $failMethod = $testCaseReflector->getMethod('fail');
    $failMethod->setAccessible(TRUE);

    $this->omitMockeryFromFailureTraces($testCase);

    $failMethod->invoke($testCase, $failArgs);
  }

  /**
   * Adds Mockery to the list of classes a test case omits from failure traces.
   *
   * Otherwise, <code>DrupalMockery->failTestCase()</code> instead of the name
   * of the test will appear in the test results next to failures.
   *
   * @param \DrupalTestCase $testCase
   *   The test case being executed.
   */
  protected function omitMockeryFromFailureTraces(\DrupalTestCase $testCase) {
    $testCaseReflector = new \ReflectionClass($testCase);

    $skipClassesProperty = $testCaseReflector->getProperty('skipClasses');
    $skipClassesProperty->setAccessible(TRUE);

    $currentClassName = get_class();
    $skipClasses = $skipClassesProperty->getValue($testCase);

    if (!key_exists($currentClassName, $skipClasses)) {
      $skipClasses[$currentClassName]  = TRUE;

      // We have to omit reflection from traces, since that's what we use in
      // failTestCase().
      $skipClasses['ReflectionMethod'] = TRUE;

      $skipClassesProperty->setValue($testCase, $skipClasses);
    }
  }

  /**
   * Gets the cache key used to save the state of this mock factory.
   *
   * The cache key is based on the name of the factory.
   *
   * @return string
   *   The cache key.
   */
  protected function getCacheKey() {
    return 'mockery_test_mocks_' . $this->name;
  }

  /**
   * Sets-up the mock object used for functions outside of classes.
   */
  protected function setupGlobalFunctionMock() {
    $this->mock(\DrupalMockery::GLOBAL_MOCK_NAME);
  }
}
