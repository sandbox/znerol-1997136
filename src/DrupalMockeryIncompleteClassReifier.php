<?php
use \Mockery\Container;
use \Mockery\Generator\Generator;
use \Mockery\Generator\CachingGenerator;
use \Mockery\Generator\MockDefinition;
use \Mockery\MockInterface;

/**
 * This class provides the logic necessary to fully load an incomplete mock.
 *
 * Incomplete mocks (i.e. mock objects that have a type of
 * `__PHP_Incomplete_Class`) result from a mock being serialized and saved in
 * the Drupal cache, then restored in a separate request without the object base
 * class and mock base classes being defined yet.
 *
 * This class is able to work around the issue by extracting the necessary
 * context from a partially-loaded mock in order to define the missing classes
 * and unserialize the object properly.
 */
class DrupalMockeryIncompleteClassReifier {
  /**
   * This is the prefix that PHP adds to protected fields when an object is
   * coerced into an array. Presumably, the NULL bytes are present to make it
   * difficult for developers to tamper with the fields directly.
   */
  const PROTECTED_PREFIX = "\0*\0";

  /**
   * Gets the fully-loaded version of the specified mock object.
   *
   * This ensures that the necessary base and mock classes are defined and
   * loaded, then returns the fully-hydrated, fully-loaded mock.
   *
   * @param object $mock
   *   The mock object to fully load.
   *
   * @return MockInterface
   *   The fully loaded mock.
   */
  public function fullyLoadMock($mock) {
    // Do some acrobatics to load mock classes defined during set-up
    if (!$this->wasClassLoaded($mock)) {
      $mock = $this->defineMockClassAndReloadMock($mock);
    }

    return $mock;
  }

  /**
   * Takes in a partially de-serialized, incomplete mock and loads it fully.
   *
   * FIXME: This is a kludge that breaks encapsulation of the third-party
   * Mockery API.
   *
   * This only works as long as ALL of the following assumptions hold:
   * 1) the serialized data for the incomplete mock includes the Mockery
   *    Container.
   * 2) the serialized data for the Container includes the Generator
   * 3) the Container uses a CachingGenerator for definition lookup.
   * 4) the CachingGenerator continues to have a protected field called
   *    "cache" that contains all of the mock definitions.
   *
   * @param object $mock
   *   The incomplete mock object.
   *
   * @return MockInterface
   *   The fully-loaded mock object.
   */
  protected function defineMockClassAndReloadMock($mock) {
    $mockData = (array)$mock;
    $missingClassName = $mockData['__PHP_Incomplete_Class_Name'];

    $container = $this->getContainerFromMock($mock);

    if (empty($container)) {
      throw new UnexpectedValueException(
        'There is no mock container for the provided mock class: ' .
        $missingClassName);
    }

    $definition =
      $this->getDefinitionFromContainer($missingClassName, $container);

    if (empty($definition)) {
      throw new UnexpectedValueException(
        'Could not locate a mock definition for the provided mock class: ' .
        $missingClassName);
    }

    $this->ensureTargetClassDefined($definition, $container);

    $loader = new \Mockery\Loader\EvalLoader();
    $loader->load($definition);

    // Reload the class
    $reloadedMock = unserialize(serialize($mock));

    return $reloadedMock;
  }

  /**
   * Extracts the mock container from the state of a mock object.
   *
   * This method accepts a mock object that is partially incomplete after
   * deserialization (i.e. it is of type <code>__PHP_Incomplete_Class</code>),
   * as this method is needed to obtain the mock definition for de-serializing a
   * complete copy of the mock.
   *
   * @param object $mockObject
   *   The mock object, which is often of type __PHP_Incomplete_Class.
   *
   * @return Container|NULL
   *   Either the container; or, <code>NULL</code> if one cannot be extracted
   *   from the mock object's state.
   */
  protected function getContainerFromMock($mockObject) {
    return $this->getProtectedValue($mockObject, '_mockery_container');
  }

  /**
   * Extracts a mock definition for a class from the state of a mock container.
   *
   * This can only be used if the mock container already has the definition
   * cached. For example, a container that was de-serialized with the definition
   * cached.
   *
   * @param string $mockClassName
   *   The name of the mock class, as was defined by Mockery. This is often
   *   of the format "Mockery_X__MOCK_CLASS_NAME", where X indicates the index
   *   of the Mockery and "MOCK_CLASS_NAME" is the name of the class, as
   *   provided by the test.
   * @param Container $container
   *   A mock container that contains a cached definition for the mock class.
   *
   * @return MockDefinition|NULL
   *   Either the desired definition; or, <code>NULL</code> if one cannot be
   *   extracted from the mock object's state.
   */

  protected function getDefinitionFromContainer($mockClassName,
                                                Container $container) {
    $targetDefinition = NULL;

    /* @var Generator $generator */
    $generator = $container->getGenerator();

    if ($generator instanceof CachingGenerator) {
      $definitions = $this->getProtectedValue($generator, 'cache');

      foreach ($definitions as $definition) {
        /* @var Mockery\Generator\MockDefinition $definition */
        $config = $definition->getConfig();

        if ($config->getName() == $mockClassName) {
          $targetDefinition = $definition;
          break;
        }
      }
    }

    return $targetDefinition;
  }

  /**
   * Ensures that the target class for a mock is defined.
   *
   * When a mock object is being constructed, if no class exists that has a
   * name that matches what was provided for the target class, Mockery
   * stubs a parent class to use as a target. Unfortunately, when we are
   * restoring the state of a mock during de-serialization, such a stub will not
   * exist. This method ensures that it does.
   *
   * @param MockDefinition $definition
   *   The mock definition for which a target class may need to be defined.
   * @param Container $container
   *   The mock container to use to define the class.
   */
  protected function ensureTargetClassDefined(MockDefinition $definition,
                                              Container $container) {
    $config = $definition->getConfig();
    $targetClassName = $config->getTargetClassName();

    if (($config->getName() != ltrim($targetClassName, '\\')) &&
      !class_exists($targetClassName)) {
      $container->declareClass($targetClassName);
    }
  }

  /**
   * Extracts a protected value from the instance of a class.
   *
   * As this breaks encapsulation, this utility method should be used only as a
   * last resort if there is no other way to obtain the necessary information.
   *
   * For our purposes here, this implementation is preferred over using similar
   * functionality available via <code>\ReflectionClass</code> because it can
   * extract properties from partially incomplete objects (i.e. objects that
   * were de-serialized without their corresponding class definition and now
   * indicate their type as <code>__PHP_Incomplete_Class</code>).
   *
   * @param object $object
   *   The object from which a protected value is desired.
   * @param string $propertyName
   *   The name of the property in the object, as it appears in the code.
   * @param mixed $defaultValue
   *   An optional parameter to specify what value should be returned if the
   *   property is missing.
   *
   * @return mixed
   *   Either the value of the desired property; or, the default value if the
   *   property was not found.
   */
  protected function getProtectedValue($object, $propertyName,
                                       $defaultValue = NULL) {
    // Coercing an object to an array allows us to work with incomplete objects
    $objectArray = (array)$object;
    $propertyKey = self::PROTECTED_PREFIX . $propertyName;

    if (isset($objectArray[$propertyKey])) {
      $result = $objectArray[$propertyKey];
    }
    else {
      $result = $defaultValue;
    }

    return $result;
  }

  /**
   * Determines whether or not a mock object was properly deserialized.
   *
   * A PHPIncompleteClass object can't be accessed directly. Calling
   * <code>is_object()</code> on such an object will return FALSE.
   *
   * SOURCE:
   * https://stackoverflow.com/questions/965611/forcing-access-to-php-incomplete-class-object-properties
   *
   * @param mixed $mock
   *   The mock object.
   *
   * @return bool
   *   Whether or not the class for the provided mock object was loaded
   *   successfully.
   */
  protected function wasClassLoaded($mock) {
    return (is_object($mock) || (gettype($mock) != 'object'));
  }
}
