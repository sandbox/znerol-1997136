<?php
use \Mockery\MockInterface;

/**
 * A class for serializing and de-serializing closures used by mocks.
 *
 * This functionality is necessary for mocks that use closures in argument
 * expectations and return value generation to work properly with the
 * serialization and deserialization of mocks that happens as part of using
 * Mockery in SimpleTest integration tests.
 */
class DrupalMockeryClosureSerializer {
  private $closureSerializer;
  private $injectableExpectationSerializer;
  private $injectableExpectationDeserializer;

  /**
   * @var \DrupalMockeryClosureSerializer
   */
  private static $instance;

  /**
   * Detects whether or not the ability to serialize closures is available.
   *
   * This relies on the third-party Super Closure library to have been
   * installed with Composer or other methods.
   *
   * @return bool
   *   Whether or not closure serialization is available.
   */
  public static function isSupported() {
    return class_exists('\SuperClosure\Serializer');
  }

  /**
   * Gets or creates an instance of a serializer.
   *
   * Serializers can be expensive to construct. Since the internal state of a
   * serializer is not modified by serializing or deserializing a mock, the
   * same instance can be shared multiple times, instead of constructing a new
   * instance each time.
   */
  public static function getInstance() {
    if (empty(self::$instance)) {
      self::$instance = new \DrupalMockeryClosureSerializer();
    }

    return self::$instance;
  }

  /**
   * Constructor for DrupalMockeryClosureSerializer.
   *
   * Consider using getInstance() instead, unless you absolutely need to
   * construct a separate instance from the shared instance.
   */
  public function __construct() {
    $this->closureSerializer = new \SuperClosure\Serializer();

    $this->injectableExpectationSerializer =
      $this->createInjectableExpectationSerializer();

    $this->injectableExpectationDeserializer =
      $this->createInjectableExpectationDeserializer();
  }

  /**
   * Serializes all closures in all expectations of the provided mocks.
   *
   * The closures are serialized in-place, causing the original closures to be
   * removed from the objects and replaced by their serialized form. Once this
   * method has been called on mocks, the mock instances cannot be used until
   * deserializeClosuresInMocks() is called on them.
   *
   * @param MockInterface[] $mocks
   *   The mocks for which closures will be serialized in-place.
   */
  public function serializeClosuresInMocks(array $mocks) {
    foreach ($mocks as $mock) {
      /* @var MockInterface $mock */
      $this->serializeClosuresInMock($mock);
    }
  }

  /**
   * Serializes all closures in all expectations of the provided mock.
   *
   * The closures are serialized in-place, causing the original closures to be
   * removed from the objects and replaced by their serialized form. Once this
   * method has been called on a mock, that mock instance cannot be used until
   * deserializeAllClosures() is called on them.
   *
   * @param MockInterface $mock
   *   The mock for which closures will be serialized in-place.
   */
  public function serializeClosuresInMock(MockInterface $mock) {
    $methodDirectors = $mock->mockery_getExpectations();

    foreach ($methodDirectors as $method => $director) {
      /* @var \Mockery\ExpectationDirector $director */
      $methodExpectations = $director->getExpectations();

      $this->serializeClosuresInExpectations($methodExpectations);
    }
  }

  /**
   * Serializes all closures in the provided expectations.
   *
   * The closures are serialized in-place, causing the original closures to be
   * removed from the objects and replaced by their serialized form. Once this
   * method has been called on expectations, those expectations will fail to
   * function properly until deserializeClosuresInExpectations() is called on
   * them.
   *
   * @param \Mockery\Expectation[] $expectations
   *   The mocks for which closures will be serialized in-place.
   */
  public function serializeClosuresInExpectations(array $expectations) {
    foreach ($expectations as $expectation) {
      /* @var \Mockery\Expectation $expectation */
      $serialize =
        $this->injectableExpectationSerializer->bindTo(
          $expectation, $expectation);

      $serialize();
    }
  }

  /**
   * De-serializes all closures in all expectations in the provided mocks.
   *
   * The closures are de-serialized in-place, restoring the original closures
   * and making the mock objects once again able to respond as expected. The
   * serialized form of the closures is eliminated in the process.
   *
   * @param MockInterface[] $mocks
   *   The mocks for which closures will be de-serialized in-place.
   */
  public function deserializeClosuresInMocks(array $mocks) {
    foreach ($mocks as $mock) {
      /* @var MockInterface $mock */
      $this->deserializeClosuresInMock($mock);
    }
  }

  /**
   * De-serializes all closures in all expectations in the provided mock.
   *
   * The closures are de-serialized in-place, restoring the original closures
   * and making the mock object once again able to respond as expected. The
   * serialized form of the closures is eliminated in the process.
   *
   * @param MockInterface $mock
   *   The mocks for which closures will be de-serialized in-place.
   */
  public function deserializeClosuresInMock(MockInterface $mock) {
    $methodDirectors = $mock->mockery_getExpectations();

    foreach ($methodDirectors as $method => $director) {
      /* @var \Mockery\ExpectationDirector $director */
      $methodExpectations = $director->getExpectations();

      $this->deserializeClosuresInExpectations($methodExpectations);
    }
  }

  /**
   * De-serializes all closures in the provided expectations.
   *
   * The closures are de-serialized in-place, restoring the original closures
   * and making the expectations once again able to respond as expected. The
   * serialized form of the closures is eliminated in the process.
   *
   * @param \Mockery\Expectation[] $expectations
   *   The mocks for which closures will be serialized in-place.
   */
  public function deserializeClosuresInExpectations(array $expectations) {
    foreach ($expectations as $expectation) {
      /* @var \Mockery\Expectation $expectation */
      $deserialize =
        $this->injectableExpectationDeserializer->bindTo(
          $expectation, $expectation);

      $deserialize();
    }
  }

  /**
   * Creates a closure that is injectable into an Expectation for serialization.
   *
   * @return Closure
   *   The injectable serialization closure.
   */
  protected function createInjectableExpectationSerializer() {
    $closureSerializer = $this->closureSerializer;
    $matcherSerializer = $this->createInjectableMatcherSerializer();

    $injectableSerializer =
      function() use ($closureSerializer, $matcherSerializer) {
        /* @var \SuperClosure\Serializer $closureSerializer */

        $closures = $this->_closureQueue;

        // NOTE: We have to ensure this is NULL-ed out, even if it's already
        // been serialized during this request, because otherwise serializing
        // the mockery a second time will fail.
        $this->_closureQueue = NULL;

        if (empty($this->_serializedClosureQueue)) {
          $serializedClosures = array();

          // FIXME: We can't serialize closures that were already unserialized
          // by Super Closure. So, once we've serialized the closures for an
          // expectation once, we can't do it again for the same expectation.
          if (!empty($closures)) {
            foreach ($closures as $closure) {
              /* @var \Closure $closure */
              $serializedClosures[] = $closureSerializer->serialize($closure);
            }

            $this->_serializedClosureQueue = $serializedClosures;
          }
        }

        if (!empty($this->_expectedArgs)) {
          foreach ($this->_expectedArgs as $arg) {
            if ($arg instanceof \Mockery\Matcher\MatcherAbstract) {
              $serializeArg = $matcherSerializer->bindTo($arg, $arg);
              $serializeArg();
            }
          }
        }
      };

    return $injectableSerializer;
  }

  /**
   * Creates a closure that is injectable into an Expectation for
   * de-serialization.
   *
   * @return Closure
   *   The injectable de-serialization closure.
   */
  protected function createInjectableExpectationDeserializer() {
    $closureSerializer   = $this->closureSerializer;
    $matcherDeserializer = $this->createInjectableMatcherDeserializer();

    $injectableDeserializer =
      function() use ($closureSerializer, $matcherDeserializer) {
        /* @vary \SuperClosure\Serializer $closureSerializer */

        if (!empty($this->_expectedArgs)) {
          foreach ($this->_expectedArgs as $arg) {
            if ($arg instanceof \Mockery\Matcher\MatcherAbstract) {
              $deserializeArg = $matcherDeserializer->bindTo($arg, $arg);
              $deserializeArg();
            }
          }
        }

        if (!empty($this->_serializedClosureQueue)) {
          $deserializedClosures = array();

          foreach ($this->_serializedClosureQueue as $serializedClosure) {
            /* @var \Closure $closure */
            $closure = $closureSerializer->unserialize($serializedClosure);
            $deserializedClosures[] = $closure;
          }

          $this->_closureQueue = $deserializedClosures;
        }
      };

    return $injectableDeserializer;
  }

  /**
   * Creates a closure that is injectable into a matcher, for serialization of
   * closure values in matchers.
   *
   * @return Closure
   *   The injectable serialization closure.
   */
  protected function createInjectableMatcherSerializer() {
    $closureSerializer = $this->closureSerializer;

    $injectableSerializer = function() use ($closureSerializer) {
      /* @var \SuperClosure\Serializer $closureSerializer */

      if ($this->_expected instanceof \Closure) {
        $closure = $this->_expected;

        // NOTE: We have to ensure this is NULL-ed out, even if it's already
        // been serialized during this request, because otherwise serializing
        // the mockery a second time will fail.
        $this->_expected  = NULL;

        if (empty($this->_serializedExpected)) {
          $this->_serializedExpected = $closureSerializer->serialize($closure);
        }
      }
    };

    return $injectableSerializer;
  }

  /**
   * Creates a closure that is injectable into a matcher, for de-serialization
   * of closure values in matchers.
   *
   * @return Closure
   *   The injectable de-serialization closure.
   */
  protected function createInjectableMatcherDeserializer() {
    $closureSerializer = $this->closureSerializer;

    $injectableDeserializer = function() use ($closureSerializer) {
      /* @var \SuperClosure\Serializer $closureSerializer */

      if (!empty($this->_serializedExpected)) {
        /* @var \Closure $this->_serializedExpected */
        $this->_expected =
          $closureSerializer->unserialize($this->_serializedExpected);
      }
    };

    return $injectableDeserializer;
  }
}
