<?php
use \Mockery\MockInterface;

/**
 * A proxy around a mock object being maintained by a Drupal mock factory.
 *
 * Drupal SimpleTest web test cases are actually spread across separate threads
 * (i.e. separate requests). SimpleTest involves creating mock objects in the
 * test setup that must be accessible to the test code in a separate request.
 * The results must then be available in the main test thread to check
 * that the code under test performed as expected (i.e. to verify expectations).
 * Needless to say, the Mockery library was not designed with these
 * cross-request use cases in mind.
 *
 * To support these use cases, mock objects actually get de-serialized, updated,
 * serialized, and stashed in the Drupal cache frequently throughout a test.
 * For consistency, all mock objects and their proxies are serialized together
 * as part of the mock factory's data bundle during this process.
 *
 * Without using proxy objects, two problems would occur:
 *
 * 1) deserialization of a mock object may fail in a new request because there
 * is not yet a definition for the mock class (since the class doesn't actually
 * exist -- the object is just a mock with a class that was declared on the
 * fly).
 *
 * 2) the data in the mock object instance of the main test thread can become
 * stale, as the data of interest (call counts, call order, etc) is actually
 * being updated in request threads and stashed in the Drupal cache.
 *
 * To remedy this, proxy objects perform method call dispatch by looking up mock
 * objects in the mock factory by name and then forwarding on calls. If a mock
 * object is unserialized without its class definition, the mockery will load
 * the appropriate mock class definition from the state of the partially
 * unserialized object and then attempt to unserialize the object again before
 * providing the object on which to invoke the forwarded call.
 *
 * @see DrupalMockery::save()
 * @see DrupalMockery::restore()
 */
class DrupalMockeryMockProxy {
  protected $mockeryName;
  protected $mockLabel;

  /**
   * Constructor for DrupalMockeryMockProxy.
   *
   * The new instance will act as a proxy for the the specified mock in the
   * specified mock factory.
   *
   * @param string $mockeryName
   *   The name of the mock factory that has generated the mock object.
   * @param string $mockLabel
   *   The name of the mock object for which this object is a proxy.
   */
  public function __construct($mockeryName, $mockLabel) {
    $this->mockeryName = $mockeryName;
    $this->mockLabel = $mockLabel;
  }

  /**
   * Magic function that forwards ("proxies") calls to the mock object.
   *
   * @param string $func
   *   The name of the function being invoke.
   * @param array $args
   *   The arguments being passed to the function.
   *
   * @return mixed
   *   The result (if any) of the call.
   */
  public function __call($func, $args) {
    $mock = $this->getTargetMock();
    $targetMethod = array($mock, $func);

    return call_user_func_array($targetMethod, $args);
  }

  /**
   * Magic function for controlling the fields serialized in Drupal's cache.
   *
   * We only want to save the name of the mockery and mock object, since all
   * mock object lookups have to happen on-the-fly for consistency.
   *
   * @return array
   *   The list of fields in this object that should be serialized.
   */
  public function __sleep() {
    return array('mockeryName', 'mockLabel');
  }

  /**
   * Gets the actual mock object that this is a proxy for.
   *
   * @return MockInterface
   */
  public function getTargetMock() {
    $mockery = DrupalMockery::mockery($this->mockeryName);

    $mockLabel = $this->mockLabel;

    return $mockery->getMock($mockLabel);
  }
}
