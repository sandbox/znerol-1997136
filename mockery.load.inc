<?php

// Load mockery and hamcrest frameworks. Please install them using PEAR:
//
//   sudo pear channel-discover pear.survivethedeepend.com
//   sudo pear channel-discover hamcrest.googlecode.com/svn/pear
//   sudo pear install --alldeps deepend/Mockery
//
// When those frameworks are installed outside the include_path, you may write
// and supply an alternative loader by specifying it in your settings.php:
//
//   $conf['mockery_load_inc'] = '/path/to/your/own/mockery.load.inc';
//
// NOTE: Super Closure cannot currently be loaded using this file. You will need
//       to use the "Hammock" module for SC support.


define('MOCKERY_LOADER_FILENAME', 'Mockery/Loader.php');
define('HAMCREST_LOADER_FILENAME', 'Hamcrest/Hamcrest.php');

if (file_exists(MOCKERY_LOADER_FILENAME) ||
    stream_resolve_include_path(MOCKERY_LOADER_FILENAME)) {
  require_once(MOCKERY_LOADER_FILENAME);

  $loader = new \Mockery\Loader;
  $loader->register();
}

if (file_exists(HAMCREST_LOADER_FILENAME) ||
  stream_resolve_include_path(HAMCREST_LOADER_FILENAME)) {
  require_once(HAMCREST_LOADER_FILENAME);
}
