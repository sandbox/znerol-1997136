<?php

/**
 * List features for the mockery mock object framework
 *
 * @return array
 *   List of translated strings.
 */
function hook_mockery_subject_list_features($arg = NULL) {
  return array(
    t('Simple'),
    t('Flexible'),
  );
}
