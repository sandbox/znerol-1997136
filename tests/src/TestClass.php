<?php
/**
 * A dummy class for testing mocks.
 */
class TestClass {
  public function getOutput() {
    return array('1' => 'A', '2' => 'B', '3' => 'C');
  }
}
